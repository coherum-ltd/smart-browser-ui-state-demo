{
  Demo of the BrowserUiState SmartPascal bindings
  Copyright (C) 2021  Nikola Dimitrov
    
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

  This software uses the library
  [browser-ui-state](https://www.npmjs.com/package/browser-ui-state).
  Refer to [LICENSE](https://github.com/device-hackers/browser-ui-state/blob/master/LICENSE)
}

unit Form1;

interface

uses
  BrowserUiState,
  SmartCL.Application,
  SmartCL.Borders,
  SmartCL.Components,
  SmartCL.Controls.EditBox,
  SmartCL.Controls.Label,
  SmartCL.Controls.Memo,
  SmartCL.Controls.Panel,
  SmartCL.Device.Storage,
  SmartCL.FileUtils,
  SmartCL.Fonts,
  SmartCL.Forms,
  SmartCL.Graphics,
  SmartCL.System,
  SmartCL.Theme,
  SmartCL.Time,
  System.Device.Storage,
  System.IOUtils,
  System.JSON,
  System.Objects,
  System.Time,
  System.Types,
  System.Types.Convert,
  W3C.HTML5;

type
  TForm1 = class(TW3Form)
  private
    {$I 'Form1:intf'}
    BrowserState: JBrowserUiState;
  protected
    procedure InitializeForm; override;
    procedure InitializeObject; override;
    procedure Resize; override;

    procedure PrintBrowserState;
  public

  end;

implementation

{ TForm1 }

procedure TForm1.InitializeForm;
var
   initialOrientation: String;
begin
  inherited;

  Self.NativeScrolling := True;

  If (Window.innerWidth > Window.innerHeight)
  then
  initialOrientation := 'LANDSCAPE'
  else
  initialOrientation := 'PORTRAIT';

  BrowserState := JBrowserUiState.Create(initialOrientation, Window);
  PrintBrowserState;
end;

procedure TForm1.InitializeObject;
begin
  inherited;
  {$I 'Form1:impl'}
end;
 
procedure TForm1.Resize;
begin
  inherited;

  Self.PrintBrowserState;
end;

procedure TForm1.PrintBrowserState;
begin
  Self.EBOrientation.Text := Self.BrowserState.Orientation;
  Self.EBScreenAspectRatio.Text := FloatToStr(BrowserState.ScreenAspectRatio);
  Self.EBViewportAspectRatio.Text := FloatToStr(BrowserState.ViewportAspectRatio);
  Self.EBDelta.Text := IntToStr(BrowserState.Delta);
  Self.EBDeviation.Text := FloatToStr(BrowserState.Deviation);

  If BrowserState.CollapsedThreshold <> Null Then
     Self.EBScreenAspectRatio.Text := FloatToStr(BrowserState.CollapsedThreshold);

  If BrowserState.KeyboardThreshold <> Null  Then
    Self.EBKeyboardThreshold.Text := FloatToStr(BrowserState.KeyboardThreshold);

  Self.EBState.Text := BrowserState.State;

  MFScreen.Text := JSON.Stringify(BrowserState.FScreen);
end;
 
initialization
  Forms.RegisterForm({$I %FILE%}, TForm1);
end.